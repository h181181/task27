﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstProjectAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FirstProjectAPI.Controller
{
    [Route("api/supervisor")]
    [ApiController]
    public class FirstWebAPIController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public FirstWebAPIController(SupervisorContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetASupervisor()
        {
            return _context.Supervisors;
        }
    }
}